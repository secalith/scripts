# Install new DB
mysql_create_database_response=
read -p "Do you want to create new Database? [y/N] " mysql_create_database_response;
case "$mysql_create_database_response" in
	[yY][eE][sS]|[yY])
		# Get database name
		mysql_database_name=
		read -p "Enter Database Name: " mysql_database_name;
		mysql -u root -p -e "CREATE DATABASE $mysql_database_name";
		echo "Created database '$mysql_database_name'";
		mysql -u root -p -"USE $mysql_database_name;";
		# Prompt if should create user
		mysql_create_user_response=
		read -p "Do you want to create new DB user and grant all priviledges? [y/N] " mysql_create_user_response;

		case "$mysql_create_user_response" in
			[yY][eE][sS]|[yY])
				# prompt for username
				mysql_user_name=
				read -p "Enter username: " mysql_user_name;
				# prompt for DB user password
				mysql_user_password=
				read -p "Enter password for new user: " mysql_user_password;
				mysql -uroot -p -e "GRANT ALL PRIVILEGES ON $mysql_database_name.* To '$mysql_user_name'@'localhost' IDENTIFIED BY '$mysql_user_password'; FLUSH PRIVILEDGES;";
				;;
			*)
			# user selected NO
				;;
		esac

		;;
	*)
		echo "NO!"
	;;
esac


