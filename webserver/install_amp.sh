#!/bin/bash

apt-get update

apt-get install zip -y
apt-get install composer -y

# install Apache2
apt-get install apache2 -y
apt-get install apache2-utils -y
a2enmod rewrite
a2enmod ssl
service apache2 restart

# In order to prepare for PHP 7.1 installation execute the following command
apt-get install -y python-software-properties
add-apt-repository -y ppa:ondrej/php
apt-get update -y

# Install PHP 7.1 with modules
sudo apt-get install -y php7.1 libapache2-mod-php7.1 php7.1-cli php7.1-common php7.1-mbstring php7.1-gd php7.1-intl php7.1-xml php7.1-mysql php7.1-mcrypt php7.1-zip

#Enable PHP modules
phpenmod mbstring
phpenmod gd
phpenmod intl
phpenmod xml
#phpenmod mysql
phpenmod mcrypt
phpenmod zip

# Install MySQL 5.7
apt-get install mysql-server-5.7 -y
mysql_secure_installation

# Install new DB
sh ./setup_database.sh
